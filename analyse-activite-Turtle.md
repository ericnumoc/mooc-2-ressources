#OBJECTIF : INTRODUCTION AU LANGAGE DE PROGRAMMATION PYTHON À L’AIDE DU MODULE TURTLE  
#PRÉ-REQUIS : être à l’aise avec l’outil informatique, notions en langage de programmation, avoir un interpréteur python  
#DURÉE DE L’ACTIVITÉ : 1 heure par groupe d'exercices  
#EXERCICES CIBLES  
##Test d’un exemple de commandes python  
Tracés de différents carrés
###Tracés de cristaux  
Tracés de polygones
Tracé du cercle
####Dessiner différents motifs à base de formes carrées à l’aide du module turtle  
#####Dessiner différents motifs à base de plusieurs formes grâce au module turle  
#DÉROULÉ DE L’ACTIVITÉ  
*Pédagogie démonstrative exposant les commandes python  
*Progression graduelle dans la complexité des commandes à mettre en œuvre  
*Pédagogie active appliquée dans les exercices proposés aux apprenants  
#REMÉDIATIONS AUX DIFFICULTÉS D’APPRENTISSAGE  
*Les élèves transcrivent-ils les concepts exposés dans les exercices qui leur sont demandés ?  
*Les élèves sont-ils à l’aise avec la logique ?  
*La progression en complexité est-elle suffisamment adaptée : en termes de temps passé sur chaque commande ? En terme de zone proximale de développement ? En terme de compréhension des énoncés d’exercice ?  
*Résolution par transfert, accompagnement individualisé  
#GESTION DE L'HÉTÉROGÉNÉITÉ 
